package com.rb.tests.utils

import scala.concurrent.duration._
import scala.io.Source._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


class LDSConverterPerformanceTests extends Simulation {

  val httpProtocol = http
    .baseURL("https://rbcom-glds-test.azurewebsites.net")
    .inferHtmlResources()
    .acceptHeader("*/*")
    .connectionHeader("close")

  val headers_0 = Map(
    "Content-Type" -> "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "accept-encoding" -> "gzip, deflate",
    "cache-control" -> "no-cache",
    "glds.accesstoken" -> "BC10076C-839E-45FC-BD79-7D21DDCC0761",
    "glds.allowedtags" -> "a;p;b;u;i;ul;ol;li")

  val headers_1 = Map(
    "Content-Type" -> "application/msword",
    "accept-encoding" -> "gzip, deflate",
    "cache-control" -> "no-cache",
    "glds.accesstoken" -> "BC10076C-839E-45FC-BD79-7D21DDCC0761",
    "glds.allowedtags" -> "a;p;b;u;i;ul;ol;li")

  val headers_2 = Map(
    "Content-Type" -> "application/pdf",
    "accept-encoding" -> "gzip, deflate",
    "cache-control" -> "no-cache",
    "glds.accesstoken" -> "BC10076C-839E-45FC-BD79-7D21DDCC0761",
    "glds.allowedtags" -> "a;p;b;u;i;ul;ol;li")

  val uri1 = "https://rbcom-glds-test.azurewebsites.net:443/api/convertdocumenttohtml"

  
  val scn_docx = scenario("LDSConverterPerformanceTests_DOCX")
    .exec(http("DOCX")
      .post("/api/convertdocumenttohtml")
      .headers(headers_0)
//      .body(lines)
      .body(RawFileBody("LDSConverterPerformanceTests_0000_request.txt"))
      .check(status.is(200)))

  setUp(
    scn_docx.inject(rampUsers(1) over (1 seconds)))
    .protocols(httpProtocol)
}

